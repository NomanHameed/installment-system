<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/unauthorized', function () {
    return view('auth.unauthorized')->with('role', 'ADMIN');
})->name('unauthorized');

Route::prefix('admin')->middleware(['auth', 'middleware' => 'App\Http\Middleware\AdminMiddleware'])->group(function () {
    Route::get('/dashboard', [UserController::class, 'Authenticate'])->name('dashboard');
    Route::get('/users', [RegisteredUserController::class, 'ShowUsers'])->name('users');
    Route::get('/sales', [SaleController::class, 'SaleList'])->name('sales');
});
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [UserController::class, 'Authenticate'])->name('dashboard');
    Route::get('/clients', [ClientController::class, 'ClientsList'])->name('clients-list');
    Route::get('/clients/{id}', [ClientController::class, 'clientsDetails'])->name('client-details');
    Route::get('/products', [ProductController::class, 'ProductsList'])->name('products-list');
    Route::get('/product/{id}', [ProductController::class, 'ProductDetail'])->name('product-detail');
    Route::get('/order/{id}', [ProductController::class, 'CreateOrder'])->name('create-order');
    Route::get('/sales', [SaleController::class, 'sales'])->name('sales-list');
    Route::get('/report', [ReportController::class, 'reportList'])->name('report-list');
});
require __DIR__ . '/auth.php';
