$(function () {
    // Render Categories
    function renderPosts(section, posts) {
        var selector = 'div[name="' + section + '"]';
        for (var post of posts) {
            var article_str = ``;
            article_str = `<a href="/${post.slug}" class="latest-news sidebar">
                <img src="/${post.feature_image}" alt="">
                <div class="latest-news-info">
                    <p>${post.title}</p>
                </div>
            </a>`;
            $(selector).find('.articles').append(article_str);
        }
    }

    $.ajax({
        type: 'GET',
        url: '/api/getSideBarStories',
        success: function (response) {
            console.log(response);
            for (var section in response) {
                renderPosts(section, response[section])
            }
        },
        error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
});
//# sourceMappingURL=/assets/js/sidebar.js