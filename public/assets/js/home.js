(function () {
    function renderPosts(section, posts) {
        var selector = 'div[name="' + section + '"]';
        for (var post of posts) {
            var article_str = ``;
            switch(section)
            {
                case 'must read':
                    article_str = `
                    <a href="/${post.slug}" class="must_read_item col-lg-3 col-md-6 pb-2">
                        <img src="/${post.feature_image}" alt="" class="" />
                        <div class="info">
                            <div class="title">${post.title}</div>
                            <div class="content">${post.content}</div>
                        </div>
                    </a>
                    `;
                    break;
                case 'top trending':
                    let title_of = post.title.substr(0, 100);
                    article_str = `
                        <a href="/${post.slug}" class="top_trend_item">
                            <span class="content">${title_of}</span>...
                            <span font-stle="italic">Read more</span>
                            <hr/>
                        </a>                        
                        `;
                        break;
                default:
                    article_str = `
                    <a href="/${post.slug}" class="latest-news sidebar">
                        <img src="/${post.feature_image}" alt="" class="sidebar-img">
                        <div class="latest-news-info sidebar-info">
                            <p>${post.title}</p>
                        </div>
                    </a>
                    `;
            }
            
            $(selector).find('.articles').append(article_str);
        }
    }

    function renderCategories(categories){  
        let categries_dom = '';
        for(category of categories)
        {
            let categry_dom = ``;
            if(category['posts'].length)
            {
                let post_slug = category['posts'][0].slug;
                let first_post =
                `<div class="d-flex align-items-center category-name mb-2">
                    <h6>
                        <a href="/category/${category['slug']}" class="btn-red">
                            ${category['name']}
                        </a>
                    </h6>
                </div>
                <div class="news-category pos-rel">                    
                    <a href="/${post_slug}" class=" pos-abs text-white d-block">
                        <img src="/${category['posts'][0].feature_image}" alt="">
                        <div class="gray-shade category-top-news">
                            <div class="news-caption">
                                <p class="">${category['posts'][0].title}</p>
                            </div>
                        </div>      
                    </a>                    
                </div>
                `;

                let other_posts = ``;                
                for (let i = 1; i < category['posts'].length; i++)
                {
                    post_slug = category['posts'][i].slug;
                    other_posts +=`
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <a class="news-category d-block" href="/${post_slug}">
                                ${category['posts'][i].title}
                                <p class="read-more">Read More</p>
                            </a>
                        </div>
                    </div>`;
                }
                categry_dom = first_post + other_posts;                
            }
            if(categry_dom)
            {
                categries_dom += '<div class="col-lg-3 col-md-6 category_item">' + categry_dom + '</div>';
            }
        }
        $('.categories_container').html(categries_dom);
    }

    
    $.ajax({
        type: 'GET',
        url: '/api/getHomePageStories',
        beforeSend: function (a, b) {
            //console.log( window.location.origin + b.url);
        },
        success: function (response) {
            let sections = response.sections;
            for (var section in sections) {
                renderPosts(section, sections[section])
            }
            renderCategories(response.categories);
        },
        error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
})();
//# sourceMappingURL=/assets/js/home.js