$(function (){
    $('#slug, #title').blur(function (){
        if(!$('#item_id').val())
        {
            makeSlug($(this).val());
        }
    });
    $('#slug,#title').keyup(function (){
        if(!$('#item_id').val())
        {
            makeSlug($(this).val());
        }
    });

    if($('#updated_at.create').length){
        let update_time = formatDate(Date());
        $('#updated_at.create').val(update_time);
    }


    $('#updated_at').blur(function(params) {
        let update_time = new Date(this.value);
        let is_nan = isNaN(update_time.getTime());
        if(is_nan){
            update_time = Date();
            update_time = formatDate(update_time);
            $('#updated_at').val(update_time);
        }
        else{
            console.log(update_time);
        }
    });

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = '' + d.getFullYear();
            hour = '' + d.getHours();
            minute = '' + d.getMinutes();
            second = ''+ d.getSeconds();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        if (hour.length < 2)
            hour = '0' + hour;
        if (minute.length < 2)
            minute = '0' + minute;
        if (second.length < 2)
            second = '0' + second;
        console.log(hour);
        return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
    }

    let slug_ok = false;
    let conent_given = false;

    if($('#item_id').val())
    {
        slug_ok = true;
        conent_given = true;
    }

    // $('#post_save').attr('disabled', 'disabled');

    function enable_submit(params) {
        if(conent_given && slug_ok){
            $('#post_save').removeAttr('disabled');
        }
        else{
            $('#post_save').attr('disabled', 'disabled');
        }
    }

    function makeSlug(val){
        if(val)
        {
            val = val.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
        }
        else
        {
            val = '';
        }

        let api_url = window.location.origin + '/api/verify-post-slug';
        slug_ok = false;
        enable_submit();
        $.ajax({
            url: api_url,
            data: {slug: val},
            beforeSend: function (a, params) {
                //console.log(params.url);
            },
            success: function (params) {
                if(params == 'ok')
                {
                    slug_ok = true;
                    $('#slug').removeClass('error');
                }
                else{
                    $('#slug').addClass('error');
                }
                enable_submit();
            },
            error: function (params) {
                console.log(params);
            }
        })
    }

    $('#post-categories').select2({
        width: '100%',
    });



    function tag_searching()
    {
        let prev_time_out = undefined;
        let search_url = window.location.origin + '/api/getLikeTags';
        function search_tags_ajax(term, call_back)
        {
            $.ajax({
                url: search_url,
                beforeSend: function(a, b){
                    console.log(b.url);
                },
                dataType: 'json',
                data: {term: term},
                success: call_back,
                complete: function(){
                    if(prev_time_out)
                    {
                        clearTimeout(prev_time_out);
                    }
                    console.log('done');
                }
            });
        }


        let select2_options = {
            width: '100%',
            tags: true,
            delay:990,
            ajax: {
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                transport: function (params, success, failure) {
                    let term = params.data.searchTerm;
                    if(!term)
                    {
                        return;
                    }
                    if(term.length < 3){
                        return;
                    }
                    if(prev_time_out)
                    {
                        clearTimeout(prev_time_out);
                    }
                    prev_time_out = setTimeout(function(){
                        search_tags_ajax(term, success);
                    }, 900);
                },
                processResults: function (data) {
                    let res = {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                    console.log(res, 222119);
                    return res;
                },
                cache: true
            }
        };
        $('#post-tags').select2(select2_options);
    }

    function tag_searching_without_sugguestion()
    {
        let prev_tag_count = 0;
        let prev_last_tag_value = '';

        let select2_options = {
            width: '100%',
            tags: true,
        };
        $('#post-tags').select2(select2_options);

        let tags_container = $('.tags_input .select2-container');

        $('.tags_input:first').keyup(function(e){
            if(e.keyCode == 13)
            {
                let api_url = window.location.origin + "/api/match-tags";
                let tag_count = tags_container.find('.select2-selection__rendered:first li').length;
                if(tag_count <=1)
                {
                    return;
                }
                let last_tag_li = tags_container.find('.select2-selection__rendered:first li:nth-child('+(tag_count - 1)+')');
                let last_tag_value = last_tag_li[0].childNodes[1].nodeValue;
                if(!last_tag_value)
                {
                    return;
                }

                if(tag_count == prev_tag_count && prev_last_tag_value == last_tag_value){
                    return;
                }

                prev_last_tag_value = last_tag_value;
                prev_tag_count = tag_count;

                $.ajax({
                    url:api_url,
                    data: {tag: last_tag_value},
                    success: function(data){
                        if(data == 'found')
                        {
                            last_tag_li.removeClass('warning');
                        }
                        else{
                            last_tag_li.addClass('warning');
                        }
                    }
                });
            }
        });
    }

    //tag_searching();
    tag_searching_without_sugguestion();

    if($('#contentpost').length){
        var CKEDITOR_BASEPATH = '/assets/js/';
        let ck_editor = CKEDITOR.replace( 'details' );
        ck_editor.on('change',function(e){
            if(e.editor.getData().length)
            {
                conent_given = true;
            }
            else{
                conent_given = false;
            }
            enable_submit();
        });
    }
    if($('#description').length){
        var CKEDITOR_BASEPATH = '/assets/js/';
        let ck_editor = CKEDITOR.replace( 'description' );
        ck_editor.on('change',function(e){
            if(e.editor.getData().length)
            {
                conent_given = true;
            }
            else{
                conent_given = false;
            }
            enable_submit();
        });
    }

    $('.show-later').show();

    $('#full-image').change(function() {
        var file = $('#full-image')[0].files[0].name;
        $(this).prev('label').text(file);
        readURL(this);
    });

    //Image Preview
    function readURL(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                if(input.id=='full-image'){
                    $('#full-img').attr('src', e.target.result);
                }

            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
});

$(function(){

    // if(window.File && window.FileList && window.FileReader)
    // {
        $('#files').on("change", function(event) {

            var files = event.target.files; //FileList object
            var output = document.getElementById("result");
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                //Only pics
                // if(!file.type.match('image'))
                if(file.type.match('image.*')){
                    if(this.files[i].size < 2097152){
                        // continue;
                        var picReader = new FileReader();
                        picReader.addEventListener("load",function(event){
                            var picFile = event.target;
                            var div = document.createElement("div");
                            div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" +
                                "title='preview image'/>";
                            output.insertBefore(div,null);
                        });
                        //Read the image
                        $('#clear, #result').show();
                        picReader.readAsDataURL(file);
                    }else{
                        alert("Image Size is too big. Minimum size is 2MB.");
                        $(this).val("");
                    }
                }else{
                    alert("You can only upload image file.");
                    $(this).val("");
                }
            }

        });
    // }
    // else
    // {
    //     console.log("Your browser does not support File API");
    // }
    });

$('#files').on("click", function() {
    $('.thumbnail').parent().remove();
    $('result').hide();
    $(this).val("");
});

$('#clear').on("click", function() {
    $('.thumbnail').parent().remove();
    $('#result').hide();
    $('#files').val("");
    $(this).hide();
});


