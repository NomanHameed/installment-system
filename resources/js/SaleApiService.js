import api from './Api';
import ApiService from './ApiService';
export default class SaleApiService extends ApiService{
    constructor() {
        super();
        this.api_url_tag = 'sale';
    }

    calculate(data) {
        return this.api.post('installment/calculator', data);
    }

}
