import axios from 'axios';
const api =  new axios.create({
    baseURL: 'http://localhost:8000/api/',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
    },
    // transformResponse: [function (data) {
    //     return data.data;
    // }],
})
api.baseURL = api.baseURL + 'client';
export default api;

