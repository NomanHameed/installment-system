import Model from './Model';
import Payment from './Payment';
export default class Sale extends Model{
    constructor( data = {}) {
        super();
        this.mapData(data);
        this.plans = this.plans.map(plan => new Payment(plan));
    }
    id = null
    price = null
    client = {id: null, first_name: ''}
    product = {id: null, title: '', price: 0}
    advance = null
    advance_percentage = null
    installment_months = null
    markup_percentage = null
    markup_amount = null
    amount_with_markup = null
    installment_date = null
    purchase_date = null
    status = null
    plans = []

    appendData(options) {
        return {client_id: this.client.id,
            product_id : this.product.id,
            price: this.product.price
        };
    }
    excludeParam()
    {
        return ['formData', 'merchant', 'client', 'product'];
    }
    setProduct(product){
        this.product = product;
        return this;
    }
}


