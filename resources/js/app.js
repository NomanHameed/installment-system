require('./bootstrap');

require('alpinejs');
import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.component('pagination', require('laravel-vue-pagination'));
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
export const serverURL = 'http://localhost:8000/api/';
import UsersList from './Components/Admin/UsersList';
import ClientsList from './Components/Client/ClientsList';
import ProductsList from './Components/Client/ProductsList';
import ProductDetails from './Components/Client/ProductDetails';
import ClientDetails from './Components/Client/ClientDetails';
import ClientDetailComponent from './Components/Client/ClientDetailComponent';
import ProductDetailComponent from './Components/Client/ProductDetailComponent';
import OrderPage from './Components/Client/OrderPage';
import SalesList from './Components/Client/SalesList';
import TestComponent from './Components/TestComponent';


Vue.use(BootstrapVue)
const app = new Vue({
    el: '#app',
    components: {
        TestComponent,
        UsersList,
        ClientsList,
        ProductsList,
        SalesList,
        ProductDetails,
        OrderPage,
        ClientDetails,
        ClientDetailComponent,
        ProductDetailComponent
    }
});
