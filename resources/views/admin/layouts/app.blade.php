
<!DOCTYPE html>
<html lang="en">
<head>
@include('admin.layouts.head')
    <style>
        .multi-upload
        {
            width: 80%;
            margin:auto;
            margin-top:10px;
        }
        .thumbnail{

            height: 100px;
            margin: 10px;
            float: left;
        }
        #clear{
            display:none;
        }
        #result {
            border: 4px dotted #cccccc;
            display: none;
            float: right;
            margin:0 auto;
            width: 511px;
        }
    </style>
</head>

<body >
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<div id="app">

   <!-- start loader -->
    <div id="pageloader-overlay" class="visible incoming">
      <div class="loader-wrapper-outer">
         <div class="loader-wrapper-inner">
            <div class="loader"></div>
         </div>
      </div>
   </div>

<!-- Start wrapper -->
 <div id="wrapper">
     @can('isAdmin')
         @include('admin.layouts.leftSidebar')
     @else
         @include('admin.client.layouts.leftSidebar')
     @endcan

 @include('admin.layouts.header')

<div class="clearfix"></div>

@yield('content')

   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>

  </div><!--End wrapper-->
</div>
@include('admin.layouts.footerScripts')

@stack('scripts')
</body>
<script src="{{ mix('js/app.js') }}"></script>
</html>
