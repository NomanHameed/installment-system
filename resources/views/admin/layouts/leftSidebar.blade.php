<!--Start sidebar-wrapper-->
<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo">
        <a href="{{URL::to('admin/dashboard')}}">
             <h5 class="logo-text">{{ Auth::user()->merchant->company_name }}</h5>
        </a>
    </div>
    <ul class="sidebar-menu">
        <li>
            <a class="waves-effect">
                <i class="zmdi zmdi-view-dashboard"></i> <span>Users Management</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{route('users')}}"><i class="zmdi zmdi-dot-circle-alt"></i>Users</a></li>
            </ul>
        </li>

    </ul>
    <ul class="sidebar-menu">
        <li>
            <a href="{{route('sales')}}">
                <i class="zmdi zmdi-dot-circle-alt"></i><span>Sales</span>
            </a>
        </li>
    </ul>
</div>
