<!-- Bootstrap core JavaScript-->

<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

<!-- simplebar js -->
<!-- <script src="{{ asset('assets/plugins/simplebar/js/simplebar.js') }}"></script> -->
<!-- sidebar-menu js -->
<script src="{{ asset('assets/js/sidebar-menu.js') }}"></script>
<!-- Custom scripts -->
<script src="{{ asset('assets/js/app-script.js') }}"></script>


<script src="{{ asset('assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.js') }}"></script>
<!-- Data tables -->
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
<!-- Multi Select -->
<link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#pageloader-overlay').hide();
        $('.js-example-basic-multiple').select2({
            width: '100%',
        });

        $('.js-user-basic-multiple').select2({
            width: '100%',
        });

        $('.js-role-delete-basic-multiple').select2({
            width: '100%',
        });
    });
</script>



<!-- Index js -->
<script src="{{ asset('assets/js/index.js')}}"></script>
<script src="{{ asset('assets/ckeditor_4.15.0_standard/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/ckeditor_4.15.0_standard/ckeditor/config.js') }}"></script>
<script src="{{ asset('assets/ckeditor_4.15.0_standard/ckeditor/lang/en.js') }}"></script>

<link rel="stylesheet" href="{{ asset('assets/toastr/toastr.css') }}">
<script src="{{ asset('assets/toastr/toastr.js') }}"></script>
    <script>
        toastr.options.timeOut = 0;
    </script>

    @foreach ($errors->all() as $error)
    <script>
        toastr.error('{{$error}}');
    </script>
    @endforeach

    @if (Session::has('success'))
    <script>
        toastr.success('{{Session::get('success')}}');
    </script>
    @endif
    @if (Session::has('error'))
        <script>
            toastr.error('{{Session::get('error')}}');
        </script>
    @endif
    <script>
        toastr.options.timeOut = 0;
    </script>

<script src="{{ asset('assets/js/admin_footer.js') }}"></script>
