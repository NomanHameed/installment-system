<meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="author" content=""/>
  <title>@yield('title')</title>
  <!--favicon-->

{{--  <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/x-icon"/>--}}

{{--    <link rel = "icon" href ="{{asset('gui/images/logo.png')}}" type = "image/x-icon">--}}
   <!-- Multi Select -->
{{--   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />--}}
  <!-- Vector CSS -->
{{--  <link href="{{ asset('assets/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/>--}}
  <!-- simplebar CSS-->
{{--  <link href="{{ asset('assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet"/>--}}
  <!-- Bootstrap core CSS-->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <!-- animate CSS-->
{{--  <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css"/>--}}
  <!-- Icons CSS-->
{{--  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css"/>--}}
  <!-- Sidebar CSS-->
  <link href="{{ asset('assets/css/sidebar-menu.css') }}" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="{{ asset('assets/css/app-style.css') }}" rel="stylesheet"/>
  <!-- skins CSS-->
{{--  <link href="{{ asset('assets/css/skins.css') }}" rel="stylesheet"/>--}}
  <!-- skins CSS-->
{{--  <link href="{{ asset('assets/css/skins.css') }}" rel="stylesheet"/>--}}
{{--    <script src="{{ mix('/js/app.js') }}"></script>--}}

  <!--Data Tables -->
{{--  <link href="{{ asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">--}}
{{--  <link href="{{ asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">--}}
    <!-- notifications css -->
{{--  <link rel="stylesheet" href="{{ asset('assets/plugins/notifications/css/lobibox.min.css') }}"/>--}}
{{--  <link rel="stylesheet" href="{{ asset('assets/ckeditor_4.15.0_standard/ckeditor/skins/moono-lisa/editor.css') }}"/>--}}

