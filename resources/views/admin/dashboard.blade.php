@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-12 col-lg-6 col-xl-3">
                    <div class="card gradient-deepblue">
                        <div class="card-body">
                            <h5 class="text-white mb-0"><span class="float-right"><i class="fa fa-user"></i></span></h5>
                            <p class="mb-0 text-white small-font">Total Users </p>
                          </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-3">
                    <div class="card gradient-orange">
                        <div class="card-body">
                            <h5 class="text-white mb-0">
                                {{-- {{$postCount}}  --}}
                                <span class="float-right"><i class="fa fa-pencil-square-o"></i></span></h5>
                            <p class="mb-0 text-white small-font">Total Posts </p>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-3">
                    <div class="card gradient-ohhappiness">
                        <div class="card-body">
                            <h5 class="text-white mb-0">
                                {{-- {{$categoryCount}}  --}}
                                <span class="float-right"><i class="fa fa-eye"></i></span></h5>
                            <p class="mb-0 text-white small-font">Total Categories </p>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-3">
                    <div class="card gradient-ibiza">
                        <div class="card-body">
                            <h5 class="text-white mb-0">
                                {{-- {{$tagCount}} --}}
                                 <span class="float-right"><i class="fa fa-envira"></i></span></h5>

                            <p class="mb-0 text-white small-font">Total Tags </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
