@extends('admin.layouts.app')

@section('title', 'Products')

@section('content')

    <div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('/admin')}} ">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Sale</li>
                </ol>
            </div>
        </div>
    <div class="row">
        <test-component></test-component>
{{--        <div class="col-lg-12">--}}
{{--            <div class="card-body">--}}
{{--                <div class="table-responsive">--}}
{{--                    <table id="default-datatable" class="table table-bordered">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>Name</th>--}}
{{--                            <th>Product</th>--}}
{{--                            <th>Amount</th>--}}
{{--                            <th>Installment Date</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @foreach($sales as $sale)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $sale->client->first_name }} {{ $sale->client->last_name }}</td>--}}
{{--                                <td>{{ $sale->product->title }}</td>--}}
{{--                                <td>{{ $sale->amount_with_markup }}</td>--}}
{{--                                <td>{{ $sale->installment_date }}</td>--}}

{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--               --}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    </div>
    </div>
@endsection
