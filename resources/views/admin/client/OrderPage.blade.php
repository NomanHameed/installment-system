@extends('admin.layouts.app')

@section('title', 'Order Page')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="row pt-2 pb-2">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('/dashboard')}} ">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Order Page</li>
                    </ol>
                </div>
            </div>
{{--            <order-page token="{{session()->get('token')}}" :product="" : :id="{{ $id }}"></order-page>--}}
            <div class="overlay toggle-menu"></div>
        </div>
    </div>


@endsection
