@extends('admin.layouts.app')

@section('title', 'Report')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumb-->
            <div class="row pt-2 pb-2">
                <div class="col-sm-12">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('/dashboard')}} ">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Report</li>
                    </ol>
                </div>
            </div>
            @foreach($report as $key => $value)
            <div>{{$key}} : {{ is_object($value) ? $value->total : $value }} </div>
            @endforeach
            <div class="overlay toggle-menu"></div>
        </div>
    </div>


@endsection
