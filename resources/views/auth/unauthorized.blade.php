<x-guest-layout>
    <x-auth-card>
        <div class="title m-b-md">
            You cannot access this page! This is for only '{{$role}}'"
        </div>
    </x-auth-card>
</x-guest-layout>
