<?php

namespace Database\Seeders;

use App\Models\Merchant;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::beginTransaction();
            $merchant = Merchant::create([
                'company_name' => 'Super User',
                'mobile' => '12345678',
                'address' => 'abc',
                'image' => 'test.jpg'
            ]);
            User::create([
                'name' => 'Super User',
                'merchant_id' => $merchant->id,
                'mobile' => '12345678',
                'email' => 'admin@gmail.com',
                'super_admin' => 1,
                'password' => Hash::make('admin@123'),
            ]);
            DB::commit();
        }
        catch (\Exception $ex){
            dd($ex->getMessage());
            DB::rollBack();
        }

    }
}
