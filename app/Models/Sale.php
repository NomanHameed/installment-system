<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = ['client_id', 'product_id', 'price', 'advance', 'advance_percentage',
        'installment_months', 'markup_percentage', 'markup_amount', 'amount_with_markup', 'installment_date',
        'purchase_date', 'status'];

    protected $hidden = ['created_at', 'updated_at'];

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function plans()
    {
        return $this->hasMany(Plan::class);
    }
}
