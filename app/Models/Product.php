<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = ['title', 'quantity', 'price', 'content', 'status', 'vendor_id', 'category_id', 'properties', 'image'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function product_image()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }

    public function getPropertiesAttribute()
    {
        return json_decode($this->attributes['properties']);
    }
}
