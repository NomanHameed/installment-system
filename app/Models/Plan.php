<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $fillable = ['installment_date', 'due_date', 'amount', 'status', 'sale_id'];

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function getTotalPayedAttribute()
    {
       return $this->payments()->get()->sum('amount_received');
    }

    public function saveStatus()
    {
        $payed = $this->total_payed;
        $status = 0;
        if ($this->amount == $payed) {
            $status = 1;
        }
        if ($this->amount > $payed) {
            $status = 2;
        }
        $this->update([
            'status' => $status
        ]);
    }

}
