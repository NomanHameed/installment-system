<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['first_name', 'last_name','mobile', 'cnic', 'email', 'address', 'country', 'state', 'zip', 'user_status', 'image'];

    protected $hidden = ['created_at', 'updated_at'];

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }
}
