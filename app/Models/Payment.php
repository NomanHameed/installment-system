<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['amount_received', 'plan_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function plans()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public static function booted()
    {
        static::saved(function ($payment) {
            $payment->plans->saveStatus();
        });
    }
}
