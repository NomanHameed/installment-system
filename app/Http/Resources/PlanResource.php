<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'installment_date' => $this->installment_date,
            "due_date" => $this->due_date,
            "amount" => $this->amount,
            "status" => $this->status,
            'total_payed' => $this->total_payed,
            'payments' => $this->payments
        ];
    }
}
