<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            "price" =>  $this->price,
            "content" =>  $this->content,
            "properties" => $this->properties,
            "quantity" => $this->quantity,
            "status" =>  $this->status,
            'category' => CategoryResource::make($this->category),
            'merchant' => MerchantResource::make($this->merchant),
            'vendor' => VendorResource::make($this->vendor),
            'images' => ImageResource::collection($this->product_image)

        ];
    }
}
