<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\DeleteRequest;
use App\Http\Requests\Category\ShowRequest;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        try {
            $category = auth()->user()->merchant->category()->get();
            return $this->responseWithSuccess('success', $category);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function store(StoreRequest $request)
    {
        try {
            $category = auth()->user()->merchant->category()->create($request->all());
            return $this->responseWithSuccess('success', $category);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function show(ShowRequest $request, Category $category)
    {
        try {
            return $this->responseWithSuccess('success', $category);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, Category $category)
    {
        try {
            $category->update($request->all());
            return $this->responseWithSuccess('success', $category);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(DeleteRequest $request, Category $category)
    {
        try {
            $category->delete();
            return $this->responseWithSuccess('Delete Success');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }
}
