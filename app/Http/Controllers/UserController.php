<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Jobs\CategoryJob;
use App\Jobs\SmsTemplate;
use App\Jobs\SmsTemplateJob;
use App\Models\Merchant;
use App\Models\User;

use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use DB;

class UserController extends Controller
{
    public function register(StoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $merchant = Merchant::create([
                'company_name' => $request->company_name,
                'mobile' => $request->mobile
            ]);

            $user = User::create([
                'name' => $request->name,
                'merchant_id' => $merchant->id,
                'mobile' => $request->mobile,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            event(new Registered($user));
            Auth::login($user);
            $token = $user->createToken('authToken')->accessToken;
            CategoryJob::dispatch();
            SmsTemplateJob::dispatch();
            DB::commit();
            return $this->responseWithSuccess('success', ['token' => $token, 'data' => $user]);
        } catch (\Exception $ex) {
            DB::rollback();
            return $this->responseWithException($ex);
        }
    }

    public function login(Request $request)
    {
        try {
            $data = [
                'mobile' => $request->mobile,
                'password' => $request->password
            ];
            auth()->attempt($data);
            $token = auth()->user()->createToken('authToken')->accessToken;
            return $this->responseWithSuccess('success', ['token' => $token, 'data' => auth()->user()]);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function updateUser(UpdateRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());
            return $this->responseWithSuccess('success', $user);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function updateMerchant(Request $request)
    {
        $request->validate([
            'company_name' => 'required|string|max:255',
            'mobile' => 'string|max:255|unique:merchants',
            'address' => 'string|max:255',
        ]);
        try {
            auth()->user()->merchant->update($request->all());
            return $this->responseWithSuccess('success', auth()->user());
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function forgot()
    {
        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);

        return response()->json(["msg" => 'Reset password link sent on your email id.']);
    }

    public function Authenticate(Request $request)
    {
        $token = Auth::user()->createToken('authToken')->accessToken;
        $request->session()->put('token', $token);
        $user = auth()->user();
        if ($user->super_admin === 1) {
            return view('admin.dashboard', compact(['token', 'user']));
        } else {
            return view('admin.dashboard', compact(['token', 'user']));
        }

    }

    public function usersList()
    {
        try {
            $users = User::paginate(1);
            return $this->responseWithSuccess('success', $users);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }
}
