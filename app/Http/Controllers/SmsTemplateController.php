<?php

namespace App\Http\Controllers;

use App\Http\Requests\SmsTemplate\ShowRequest;
use App\Http\Requests\SmsTemplate\StoreRequest;
use App\Http\Requests\SmsTemplate\UpdateRequest;
use App\Models\SmsTemplate;
use Illuminate\Http\Request;

class SmsTemplateController extends Controller
{
    public function index()
    {
        $template = auth()->user()->merchant->smstemplates()->get();
        if (!$template) {
            return $this->responseMessage('error', 'No Record Found');
        }
        return $this->responseWithSuccess('Success', $template);
    }

    public function store(StoreRequest $request)
    {
        try {
            $template = auth()->user()->merchant->smstemplates()->create($request->all());
            return $this->responseWithSuccess('Success', $template);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function show(ShowRequest $request, SmsTemplate $smsTemplate)
    {
        try {
            if (!$smsTemplate) {
                return $this->responseMessage('error', 'No Record Found');
            }
            return $this->responseWithSuccess('Success', $smsTemplate);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, $id)
    {
        try {
            $template = auth()->user()->merchant->smstemplates()->where('type', $id)->first();
            $template->update($request->all());
            return $this->responseWithSuccess('Success', $template);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

}
