<?php

namespace App\Http\Controllers;

use App\Http\Requests\Client\DeleteRequest;
use App\Http\Requests\Client\ShowRequest;
use App\Http\Requests\Client\StoreRequest;
use App\Http\Requests\Client\UpdateRequest;
use App\Models\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        try {
            $client = auth()->user()->merchant->client()
                ->when(!empty($request->search), function ($query) use ($request) {
                    return $query->where('first_name', 'like', "%{$request->search}%")
                        ->orWhere('last_name', 'like', "%{$request->search}%")
                        ->orWhere('cnic', 'like', "%{$request->search}%")
                        ->orWhere('email', 'like', "%{$request->search}%")
                        ->orWhere('mobile', 'like', "%{$request->search}%");
                })
                ->paginate(10);
            return $this->responseWithSuccess('success', $client);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $storeThumbanil = '';
            if ($request->hasFile('image')) {
                $image_name = $request->file('image')->getClientOriginalName();
                $filename = pathinfo($image_name, PATHINFO_FILENAME);
                $image_ext = $request->file('image')->getClientOriginalExtension();
                $storeThumbanil = $filename . '-' . time() . '.' . $image_ext;
                $request->file('image')->storeAs('public/client', $storeThumbanil);
            }
            $client = auth()->user()->merchant->client()->create(array_merge($request->except('image'), [
                'image' => $storeThumbanil
            ]));
            DB::commit();
            return $this->responseWithSuccess('success', $client);
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }
    }

    public function show(ShowRequest $request, Client $client)
    {
        try {
            return $this->responseWithSuccess('success', $client);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, Client $client)
    {
        try {
            $storeThumbanil = '';
            if ($request->hasFile('image')) {
                $image_name = $request->file('image')->getClientOriginalName();
                $filename = pathinfo($image_name, PATHINFO_FILENAME);
                $image_ext = $request->file('image')->getClientOriginalExtension();
                $storeThumbanil = $filename . '-' . time() . '.' . $image_ext;
                $request->file('image')->storeAs('public/client', $storeThumbanil);
            }
            $client->update(array_merge($request->except('image'), [
                'image' => $storeThumbanil
            ]));
            return $this->responseWithSuccess('success', $client);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(DeleteRequest $request, Client $client)
    {
        try {
            if ($client->image) {
                $preThumbnail = public_path('storage/product/' . $client->image);
                if (File::exists($preThumbnail)) {
                    unlink($preThumbnail);
                }
            }
            $client->delete();
            return $this->responseWithSuccess('Delete Success');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function ClientsList()
    {
        try {
            return view('admin.client.clients');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function clientsDetails($id)
    {
        try {
            return view('admin.client.clientDetails', compact('id'));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }


}
