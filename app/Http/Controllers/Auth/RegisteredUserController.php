<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Merchant;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'company_name' => 'required|string|max:255|unique:merchants,company_name',
            'mobile' => 'required|unique:users',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => ['required', 'min:4', 'confirmed', Rules\Password::defaults()],
        ]);

        $merchant = Merchant::create([
            'company_name' => $request->company_name
        ]);

        $user = User::create([
            'name' => $request->name,
            'merchant_id' => $merchant->id,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);
        $token = $user->createToken('authToken')->accessToken;
        return redirect(RouteServiceProvider::HOME)->with(['token' => $token]);
    }
    public function ShowUsers(){
        $users = User::with('merchant')->get();
        return view('admin.users.users', compact('users'));
    }
}
