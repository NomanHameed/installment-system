<?php

namespace App\Http\Controllers;

use App\Http\Requests\Payment\PaymentRequest;
use App\Http\Requests\Payment\ShowRequest;
use App\Http\Requests\Payment\StoreRequest;
use App\Http\Requests\Payment\UpdateRequest;
use App\Models\Payment;
use App\Models\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function index(PaymentRequest $request)
    {
        try {
            $payments = Payment::where('plan_id', $request->get('plan_id'))->get();;
            return $this->responseWithSuccess('success', $payments);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $plan = Plan::find($request->get('plan_id'));
            $payment = $plan->payments()->create($request->all());
            DB::commit();
            return $this->responseWithSuccess('Payment Add Successfully', $payment);
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }
    }

    public function show(ShowRequest $request, Payment $payment)
    {
        try {
            return $this->responseWithSuccess('Success', $payment);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, Payment $payment)
    {
        try {
            $payment->amount_received = $request->get('amount_received');
            $payment->save();
            return $this->responseWithSuccess('Payment Updated Successfully', $payment);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(ShowRequest $request, Payment $payment)
    {
        try {
            DB::beginTransaction();
            $plan = Plan::where('id', $payment->plan_id)->first();
            $plan->update([
                'status' => 0
            ]);
            $payment->delete();
            DB::commit();
            return $this->responseWithSuccess('Payment Deleted');
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }
    }
}
