<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\DeleteRequest;
use App\Http\Requests\Product\ShowRequest;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Vendor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $product = $request->user()->merchant->products()
                ->when(!empty($request->search), function ($query) use ($request) {
                    return $query->where('title', 'like', "%{$request->search}%");
                })->paginate(4);
            return $this->responseWithSuccess('success', new ProductCollection($product));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function checkOrCreateCategory($new_category)
    {
        try {
            $category = Category::where('name', $new_category)->first();
            if (!$category) {
                $slug = preg_replace('/\s+/', '_', $new_category);
                $category = new Category();
                $category->name = $new_category;
                $category->slug = $slug;
                $category->merchant_id = auth()->user()->merchant_id;
                $category->save();
            }
            return $category;
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function checkOrCreateVendor($new_vendor)
    {
        try {
            $vendor = Vendor::where('name', $new_vendor)->first();
            if (!$vendor) {
                $vendor = new Vendor();
                $vendor->name = $new_vendor;
                $vendor->merchant_id = auth()->user()->merchant_id;
                $vendor->save();
            }
            return $vendor;
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function store(StoreRequest $request)
    {
        try {
            $category = $this->checkOrCreateCategory($request->category);
            $vendor = $this->checkOrCreateVendor($request->vendor);

            $product = auth()->user()->merchant->products()->create(
                array_merge(
                    $request->except(['vendor', 'category']),
                    [
                        'vendor_id' => $vendor->id,
                        'category_id' => $category->id
                    ]
                )
            );

            if ($product) {
                if ($request->hasFile('images')) {
                    foreach ($request->file('images') as $img) {
                        $image_name = $img->getClientOriginalName();
                        $filename = pathinfo($image_name, PATHINFO_FILENAME);
                        $image_ext = $img->getClientOriginalExtension();
                        $storeImage = $filename . '-' . time() . '.' . $image_ext;
                        $img->storeAs('public/product', $storeImage);
                        $image = new ProductImage();
                        $image->product_id = $product->id;
                        $image->image = $storeImage;
                        $image->save();
                    }
                }
            }
            return $this->responseWithSuccess('Product Add Success', new ProductResource($product));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function show(ShowRequest $request, Product $product)
    {
        try {
            if (!$product) {
                return $this->responseMessage('error', 'No Record Found');
            }
            return $this->responseWithSuccess('Success', new ProductResource($product));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, Product $product)
    {
        try {
            $category = $this->checkOrCreateCategory($request->category);
            $vendor = $this->checkOrCreateVendor($request->vendor);

            $product->update(array_merge($request->except(['vendor', 'category']), [
                'vendor_id' => $vendor->id,
                'category_id' => $category->id
            ]));

            if ($request->hasFile('images') && count($request->file('images')) >= 1) {

                foreach ($request->file('images') as $img) {
                    $image_name = $img->getClientOriginalName();
                    $filename = pathinfo($image_name, PATHINFO_FILENAME);
                    $image_ext = $img->getClientOriginalExtension();
                    $storeImage = $filename . '-' . time() . '.' . $image_ext;
                    $img->storeAs('public/product', $storeImage);
                    $image = new ProductImage();
                    $image->product_id = $product->id;
                    $image->image = $storeImage;
                    $image->save();
                }
            }
            return $this->responseWithSuccess('Updated Successfully', new ProductResource($product));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function destroy(DeleteRequest $request, Product $product)
    {
        try {
            if (count($product->product_image) > 0) {
                foreach ($product->product_image as $image) {
                    $preThumbnail = public_path('product/' . $image->image);
                    if (File::exists($preThumbnail)) {
                        unlink($preThumbnail);
                    }
                }
            }
            $product->delete();
            return $this->responseWithSuccess('Deleted Successfully');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function ProductsList()
    {
        try {
            return view('admin.client.products');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function ProductDetail($id)
    {
        try {
            return view('admin.client.productDetails', compact('id'));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }


}
