<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function report()
    {
        try {
            $report = $this->getReportQuery();
            return $this->responseWithSuccess('success', $report);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function reportList(Request $request)
    {
        $report = $this->getReportQuery();
        return view('admin.client.report', compact(['report']));
    }

    private function getReportQuery() {

        $report = [];
        $report['products'] = auth()->user()->merchant->products()->count();
        $report['clients'] = auth()->user()->merchant->client()->count();
        $report['active_sales'] = auth()->user()->merchant->sale()->where('status', 0)->count();
        $report['close_sales'] = auth()->user()->merchant->sale()->where('status', 1)->count();
        $now = Carbon::now();

        $query = DB::table('payments')
            ->join('plans', 'plans.id', '=', 'payments.plan_id')
            ->join('sales', 'sales.id', '=', 'plans.sale_id')
            ->groupBy(['sales.merchant_id'])
            ->select(\DB::raw('SUM(payments.amount_received) as total'))
            ->where('sales.merchant_id', auth()->user()->merchant_id);

        $report['current_month_recovery'] = $query
            ->whereMonth('payments.created_at', $now->month)
            ->first();

        $report['last_month_recovery'] = DB::table('payments')
            ->join('plans', 'plans.id', '=', 'payments.plan_id')
            ->join('sales', 'sales.id', '=', 'plans.sale_id')
            ->groupBy(['sales.merchant_id'])
            ->select(\DB::raw('SUM(payments.amount_received) as total'))
            ->where('sales.merchant_id', auth()->user()->merchant_id)
            ->whereMonth('payments.created_at', $now->subMonth())
            ->first();

        $report['total_recovery'] =  DB::table('payments')
            ->join('plans', 'plans.id', '=', 'payments.plan_id')
            ->join('sales', 'sales.id', '=', 'plans.sale_id')
            ->groupBy(['sales.merchant_id'])
            ->select(\DB::raw('SUM(payments.amount_received) as total'))
            ->where('sales.merchant_id', auth()->user()->merchant_id)->first();
        return $report;
    }
}
