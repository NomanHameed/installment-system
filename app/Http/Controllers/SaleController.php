<?php

namespace App\Http\Controllers;


use App\Http\Requests\Sale\InstallmentRequest;
use App\Http\Requests\Sale\ShowRequest;
use App\Http\Requests\Sale\StoreRequest;
use App\Http\Requests\Sale\UpdateRequest;
use App\Http\Resources\SaleResource;
use App\Models\Plan;
use App\Models\Sale;
use Carbon\Traits\Creator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    public function index()
    {
        try {
            $sales = auth()->user()->merchant->sale()->get();
            return $this->responseWithSuccess('Success', SaleResource::collection($sales));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }
  /*TODO: validate client for merchant_id*/
    public function store(StoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $sale = auth()->user()->merchant->sale()->create(array_merge($request->all(), [
                'status' => 0
            ]));
            $installment = round(($request->amount_with_markup - $request->advance) / $request->installment_months);
            $start_date = new Carbon($request->installment_date);
            $end_date = new Carbon($request->installment_date);
            $end_date = $end_date->addDays(10);
            for ($i = 0; $i <= $request->installment_months; $i++) {
                $sale->plans()->create([
                    'installment_date' => $start_date,
                    'due_date' => $end_date,
                    'amount' => $installment,
                    'status' => 0
                ]);
                $start_date = $start_date->addMonth();
                $end_date = $start_date->copy()->addDays(10);
            }
            DB::commit();
            return $this->responseWithSuccess('Sale Added Successfully', new SaleResource($sale));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }
    }

    public function show(ShowRequest $request, Sale $sale)
    {
        try {
            return $this->responseWithSuccess('Success',  new SaleResource($sale));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function update(UpdateRequest $request, Sale $sale)
    {
        try {
            $sale->update($request->all());
            return $this->responseWithSuccess('Updated Successfully',  new SaleResource($sale));
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }
/*TODO: authorize, */
    public function destroy(ShowRequest $request, Sale $sale)
    {
        try {
            $sale->delete();
            return $this->responseWithSuccess('Delete Successfully');
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function installmentCalculator(InstallmentRequest $request)
    {
        try {
            $installment = round(($request->amount_with_markup - $request->advance) / $request->installment_months);
            return $this->responseWithSuccess('Success', $installment);
        } catch (\Exception $ex) {
            return $this->responseWithException($ex);
        }
    }

    public function completeOrder(Request $request)
    {
        try {
            DB::beginTransaction();
            $sale = auth()->user()->merchant->sale()->find($request->input('sale_id'));
            $plan = $sale->plans()->where('status', 0)->first();
            $plan->payments()->create([
                'amount_received' => $request->amount_received,
                'status' => 1
            ]);
            $plan->update([
                'status' => 1
            ]);
            $sale->update([
                'status' => 1
            ]);
            DB::commit();
            return $this->responseWithSuccess('Success', new SaleResource($sale));
        } catch (\Exception $ex) {
            DB::rollBack();
            return $this->responseWithException($ex);
        }

    }

    public function SaleList()
    {
        $sales = Sale::with('client','product')->get();
        return view('admin.Products.index', compact('sales'));
    }

    public function sales()
    {
        return view('admin.client.sales');
    }
}
