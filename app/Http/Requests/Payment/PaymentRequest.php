<?php

namespace App\Http\Requests\Payment;

use App\Models\Plan;
use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $plan = Plan::where('id',$this->request->get('plan_id'))->first();
        return auth()->user()->merchant_id === $plan->sale->merchant_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
