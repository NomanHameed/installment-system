<?php

namespace App\Http\Requests\Payment;
use App\Models\Plan;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public $payment;
    public $plan;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->payment = $this->route('payment');
        $this->plan = $this->payment->plans;
        return auth()->user()->merchant_id === $this->payment->plans->sale->merchant_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'amount_received' => 'required|integer|max:'. ($this->getPlan()->amount - ($this->getPlan()->total_payed - $this->payment->amount_received) ),
        ];
        return  $rules;
    }

    public function getPlan() : Plan
    {
        return $this->plan;
    }
}
